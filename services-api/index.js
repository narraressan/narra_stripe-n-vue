const express = require('express')
const cors = require('cors')
const http = require('http')
const bodyParser = require('body-parser')
const SuperLogin = require('superlogin')

// MISC - STRIPE
const stripe = require('stripe')

const app = express()
app.set('port', process.env.PORT || 2000)
app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))


// 4242 4242 4242 4242
app.post('/charge', function(req, res) {
	let doc = req.body
	let customer_id = null
	const gateway = stripe('sk_test_OYSNEjjwBTslbKgBWVxUUNuJ')

	gateway.customers.create({
		email: doc['email'],
		source: doc['source']
	})
	.then(function(customer) {
		customer_id = customer.id

		return gateway.charges.create({
			customer: customer.id,
			currency: 'usd',
			amount: 50, // $0.50 in cents
			description: doc['service'] + ': ' + doc['description']
		})
	}).then(function(charge) {
		res.json({
			'customer': customer_id,
			'charge': charge.id
		})
	}).catch(function(error) {
		res.json({ 'error': error })
	})
})


// start app
http.createServer(app).listen(app.get('port'))