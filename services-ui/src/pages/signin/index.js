// import components
const signin = () => import("./app");


// default routes
const routes = [
	{
		name: "signin",
		path: "/signin",
		component: signin,
	},
	{ path: "/", redirect: "/signin" },
	{ path: "*", redirect: "/signin" },
]


// vue instance
const vue = {
	props: [],
	mixins: [],
	data(){
		return {}
	},
	components: {},
	computed: {},
	methods: {
		signin: function(){
			this.request({
				url: "/auth/login",
				method: "POST",
				data: {
					username: this.$refs.email.value,
					password: this.$refs.password.value
				},
				onSuccess: this.onSuccess,
				onError: this.onError
			})
		},
		onSuccess: function(response){
			console.log(response)
		},
		onError: function(error){ this.notify("Unknown user credentials", "error") },
	},
	mounted(){},
	watch: {},
}


export { routes, vue as default }