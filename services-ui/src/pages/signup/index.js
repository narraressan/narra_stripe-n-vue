// import components
const signup = () => import("./app");


// default routes
const routes = [
	{
		name: "signup",
		path: "/signup",
		component: signup,
	}
]


// vue instance
const vue = {
	props: [],
	mixins: [],
	data(){
		return {
			services: [],
			selectedService: "NA"
		}
	},
	components: {},
	computed: {},
	methods: {
		signup: function(){
			if(this.selectedService != "NA"){

				let availedServices = {}
				availedServices[this.selectedService] = {
					availed: (new Date()).toISOString(),
					active: true
				}

				this.request({
					url: "/auth/register",
					method: "POST",
					data: {
						email: this.$refs.email.value,
						password: this.$refs.password.value,
						confirmPassword: this.$refs.password.value,
						profile: {
							firstname: this.$refs.firstname.value,
							lastname: this.$refs.lastname.value,
							streetAddress1: this.$refs.streetAddress1.value,
							streetAddress2: this.$refs.streetAddress2.value,
							city: this.$refs.city.value,
							state: this.$refs.state.value,
							zip: this.$refs.zip.value,
							contactNo: this.$refs.contactNo.value,
							services: availedServices,
							billing: []
						}
					},
					onSuccess: this.onSuccess,
					onError: this.onError
				})
			}
			else{ this.notify("Select a default service to avail", "error") }
		},
		onSuccess: function(response){
			// console.log(response.data)
			this.notify("User has been created. Log into your account now!", "info")
			this.$router.push("/signin")
		},
		onError: function(error){ this.notify("Email already registered.", "error") },
		getServices: function(){
			this.request({
				url: "/services",
				method: "GET",
				onSuccess: this.serviceList,
			})
		},
		serviceList: function(response){ this.services = response.data }
	},
	mounted(){
		this.getServices()
	},
	watch: {},
}


export { routes, vue as default }