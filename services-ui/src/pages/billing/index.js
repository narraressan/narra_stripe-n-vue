// import components
const billing = () => import("./app");
const stripeForm = () => import("../../components/stripe-form/app");
const paypalForm = () => import("../../components/paypal-form/app");


// default routes
const routes = [
	{
		name: "billing",
		path: "/billing",
		component: billing,
	}
]


// vue instance
const vue = {
	props: [],
	mixins: [],
	data(){
		return {}
	},
	components: {
		stripeForm,
		paypalForm,
	},
	computed: {},
	methods: {},
	mounted(){
		// testing purposes only
		this.$Tracker();
	},
	watch: {},
}


export { routes, vue as default }