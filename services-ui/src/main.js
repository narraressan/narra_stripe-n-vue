// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from "vue"
import App from "./App"
import router from "./router"

// Plugins
import NProgress from "vue-progressbar"
import Tracker from "./plugins/dom-tracker";

// Factories
import GlobalMixin from "./factories/global"

Vue.config.productionTip = false

Vue.mixin(GlobalMixin);
Vue.use(NProgress, {
	color: "#15628E",
	failedColor: "#9D0F44",
	thickness: "4px",
	router: true,
});
Vue.use(Tracker);


/* eslint-disable no-new */
new Vue({
  el: "#app",
  router,
  components: { App },
  template: "<App/>"
})
