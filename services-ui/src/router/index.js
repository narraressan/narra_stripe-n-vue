import Vue from "vue"
import Router from "vue-router"

// Known pages
import { routes as signin_routes } from "../pages/signin/index"
import { routes as signup_routes } from "../pages/signup/index"
import { routes as billing_routes } from "../pages/billing/index"

Vue.use(Router)

let routes = []
  .concat(billing_routes)
  .concat(signin_routes)
  .concat(signup_routes)

export default new Router({ routes })
