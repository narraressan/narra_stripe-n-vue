// extra components
import { SpringSpinner } from "epic-spinners"


// vue instance
export default {
	name: "Paypal-Form",
	props: [],
	mixins: [],
	data(){
		return {
			email: null,
			fullname: null,
			address: null,
			// MISC
			error: null,
			loading: false,
			disable: false,
			service: {
				name: this.$route.query.service,
				price: 15.00,
				sub: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod"
			}
		}
	},
	components: {
		SpringSpinner
	},
	computed: {},
	methods: {
		submit: function(){
			this.disable = true;
		},
	},
	mounted(){
		console.log("[paypal-form]: Init!");
	},
	watch: {},
}