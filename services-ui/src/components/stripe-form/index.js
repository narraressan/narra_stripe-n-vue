// extra components
import { SpringSpinner } from "epic-spinners";

// sample card: 4242 4242 4242 4242

// vue instance
export default {
	name: "Stripe-Form",
	props: [],
	mixins: [],
	data() {
		return {
			email: null,
			fullname: null,
			address: null,
			// STRIPE PLUGIN
			stripe: null,
			card: null,
			// MISC
			error: null,
			loading: false,
			disable: false,
			service: {
				name: this.$route.query.service,
				price: 15.0,
				sub:
					"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod"
			}
		};
	},
	components: {
		SpringSpinner
	},
	computed: {},
	methods: {
		stripeInit: function() {
			this.stripe = this.getStripe();
			var elements = this.stripe.elements({ locale: "auto" });

			this.card = elements.create("card", {
				style: {
					base: {
						// Add your base input styles here. For example:
						fontSize: "1.2rem",
						color: "#666"
					}
				}
			});
			this.card.mount("#stripe-card");

			this.card.addEventListener("change", ({ error }) => {
				if (error) {
					self.error = error.message;
					self.disable = false;
				} else {
					self.error = "";
				}
			});
		},
		submit: function() {
			this.disable = true;
			var ownerInfo = {
				owner: {
					address: {
						city: null,
						country: null,
						line1: this.address,
						line2: null,
						postal_code: null,
						state: null
					},
					email: this.email,
					name: this.fullname,
					phone: null
				}
			};

			var self = this;
			this.stripe
				.createSource(this.card, ownerInfo)
				.then(function(result) {
					try {
						if (result["error"]) {
							self.error = result["error"]["message"];
							self.disable = false;
						} else {
							self.error = "";
							self.submitSource(result["source"]["id"]);
						}
					} catch (err) {
						self.error = result["error"]["message"];
						self.disable = false;
					}
				});
		},
		submitSource: function(source_id) {
			this.card.unmount();
			this.loading = true;
			this.request({
				url: "/payment/charge",
				method: "POST",
				data: {
					source: source_id,
					email: this.email,
					description: "",
					// amount: this.toCents("1.01"),
					service: this.$route.query.service
				},
				onSuccess: this._onSuccess,
				onError: this._onError
			});
		},
		_onSuccess: function(response) {
			this.loading = false;
			this.notify("Payment success", "success");

			this.email = null;
			this.fullname = null;
			this.address = null;
			this.disable = false;
		},
		_onError: function(error) {
			this.loading = false;
			this.notify("Oops! payment failed to process properly", "error");
			this.disable = false;
		}
	},
	mounted() {
		console.log("[stripe-form]: Init!");

		if (this.service["name"]) {
			this.stripeInit();
		}
	},
	watch: {
		loading: function() {
			if (this.loading == false) {
				var self = this;
				setTimeout(function() {
					self.card.mount("#stripe-card");
				}, 100);
			}
		}
	}
};
